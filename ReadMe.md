**Benutzung:**

```PHP
Handler::register();
```

```PHP
<?php
$builder = new \Ostec\Error\LoggerBuilder();

Handler::register(
    $builder
        ->setClient(new \Ostec\Error\Client\File('/tmp'))
        ->setAggregator(new \Ostec\Error\Aggregator\Standard())
        ->setFormater(new \Ostec\Error\Formater\Logstash(['project' => 'frontend']))
        ->setFilter(new \Ostec\Error\Filter\Standard())
        ->build(),
    $builder
        ->setClient(new \Ostec\Error\Client\Udp('138.201.203.223', 9003))
        ->setAggregator(new \Ostec\Error\Aggregator\Light())
        ->setFormater(new \Ostec\Error\Formater\Logstash(['project' => 'frontend']))
        ->setFilter(new \Ostec\Error\Filter\Standard())
        ->build()
);

$trigger = new Ostec\Error\Trigger();
$trigger->triggerError("Error", E_USER_ERROR);
$trigger->triggerException(new RuntimeException("haste ne ges�hn"));
```