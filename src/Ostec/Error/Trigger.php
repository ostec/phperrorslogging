<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Class Trigger
 *
 * @package Ostec\Error
 */
class Trigger
{
    /**
     * @param string $message
     * @param int    $type
     */
    public function triggerError(string $message, int $type = E_USER_ERROR): void
    {
        trigger_error($message, $type);
    }

    /**
     * @param \Throwable $exception
     */
    public function triggerException(\Throwable $exception): void
    {
        Handler::getInstance()->userExceptionHandler($exception);
    }
}
