<?php declare(strict_types=1);

namespace Ostec\Error;

use Throwable;

/**
 * Interface Logger
 *
 * @package Ostec\Error
 */
interface Logger
{
    /**
     * @param int    $errno
     * @param string $errMsg
     * @param string $fileName
     * @param int    $lineNum
     * @param array  $vars
     */
    public function userErrorHandler(int $errno, string $errMsg, string $fileName, int $lineNum, ?array $vars = []): void;

    /**
     * @param Throwable $exception
     */
    public function userExceptionHandler(Throwable $exception): void;
}
