<?php declare(strict_types=1);

namespace Ostec\Error\Filter;

use Ostec\Error\Filter;

/**
 * Class Standard
 *
 * @package Ostec\Error\Filter
 */
class Standard implements Filter
{
    private static $rules = [
        E_ERROR        => 'Error',
        E_WARNING      => 'Warning',
        E_USER_ERROR   => 'User Error',
        E_USER_WARNING => 'User Warning',
    ];

    /**
     * @param int $errorType
     *
     * @return bool
     */
    public function filter(int $errorType): bool
    {
        return isset(self::$rules[$errorType]);
    }

    /**
     * @param int $errorType
     *
     * @return string
     */
    public function getName(int $errorType): string
    {
        return self::$rules[$errorType] ?? (string)$errorType;
    }
}
