<?php declare(strict_types=1);

namespace Ostec\Error\Aggregator;

use Ostec\Error\Aggregator;

/**
 * Class Exception
 *
 * @package Ostec\Error\Aggregator
 */
class Exception
{
    /**
     * @var Aggregator
     */
    private $aggregator;

    /**
     * Exception constructor.
     *
     * @param Aggregator $aggregator
     */
    public function __construct(Aggregator $aggregator)
    {
        $this->aggregator = $aggregator;
    }


    /**
     * @param \Throwable $throwable
     *
     * @return array
     */
    public function getMessage(\Throwable $throwable): array
    {
        $data = $this->aggregator->getMessage(
            get_class($throwable), $throwable->getCode(), $throwable->getMessage(), $throwable->getFile(), $throwable->getLine(), []
        );

        $data['STACKTRACE'] = $throwable->getTraceAsString();

        return $data;
    }
}
