<?php declare(strict_types=1);

namespace Ostec\Error\Aggregator;

use Ostec\Error\Aggregator;

/**
 * Class Soap
 *
 * @package Ostec\Error\Aggregator
 */
class Soap implements Aggregator
{
    /**
     * @param string $errorType
     * @param int    $errno
     * @param string $errMsg
     * @param string $fileName
     * @param int    $lineNum
     * @param array  $vars
     *
     * @return array
     */
    public function getMessage(string $errorType, int $errno, string $errMsg, string $fileName, int $lineNum, ?array $vars = []): array
    {
        return [
            'DATETIME'   => date('Y-m-d H:i:s (T)'),
            'TYPE'       => $errorType,
            'MSG'        => $errMsg,
            'SCRIPT'     => $fileName,
            'LINE'       => $lineNum,
            'SERVER'     => $_SERVER,
            'PAYLOAD'    => file_get_contents('php://input'),
            'STACKTRACE' => $this->getStackTrace(),
        ];
    }

    /**
     * @return array
     */
    private function getStackTrace(): array
    {
        $backTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        for ($cnt = 0; $cnt < 4; $cnt++) {
            array_shift($backTrace);
        }

        return $backTrace;
    }
}
