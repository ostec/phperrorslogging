<?php declare(strict_types=1);

namespace Ostec\Error\Aggregator;

use Ostec\Error\Aggregator;

/**
 * Class Standard
 *
 * @package Ostec\Error\Aggregator
 */
class Standard implements Aggregator
{
    /**
     * @var array
     */
    private $options;

    /**
     * Standard constructor.
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @param string $errorType
     * @param int    $errno
     * @param string $errMsg
     * @param string $fileName
     * @param int    $lineNum
     * @param array  $vars
     *
     * @return array
     */
    public function getMessage(string $errorType, int $errno, string $errMsg, string $fileName, int $lineNum, ?array $vars = []): array
    {
        return [
            'DATETIME'   => date('Y-m-d H:i:s (T)'),
            'TYPE'       => $errorType,
            'MSG'        => $errMsg,
            'SCRIPT'     => $fileName,
            'LINE'       => $lineNum,
            'SERVER'     => $_SERVER,
            'GET'        => $_GET,
            'POST'       => $_POST,
            'FILES'      => $_FILES,
            'STACKTRACE' => $this->getStackTrace(),
        ];
    }

    /**
     * @return array
     */
    private function getStackTrace(): array
    {
        if (isset($this->options['debug_backtrace'])) {
            if (!isset($this->options['debug_backtrace']['disable'])) {
                $backTrace = debug_backtrace(
                    $this->options['debug_backtrace']['args'],
                    $this->options['debug_backtrace']['limit'] + 5
                );

                for ($cnt = 0; $cnt < 4; $cnt++) {
                    array_shift($backTrace);
                }

                return $backTrace;
            }
        } else {
            $backTrace = debug_backtrace();

            for ($cnt = 0; $cnt < 4; $cnt++) {
                array_shift($backTrace);
            }

            return $backTrace;
        }

        return [];
    }
}
