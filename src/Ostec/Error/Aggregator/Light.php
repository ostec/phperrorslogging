<?php declare(strict_types=1);

namespace Ostec\Error\Aggregator;

use Ostec\Error\Aggregator;

/**
 * Class Light
 *
 * @package Ostec\Error\Aggregator
 */
class Light implements Aggregator
{
    /**
     * @param string $errorType
     * @param int    $errno
     * @param string $errMsg
     * @param string $fileName
     * @param int    $lineNum
     * @param array  $vars
     *
     * @return array
     */
    public function getMessage(string $errorType, int $errno, string $errMsg, string $fileName, int $lineNum, ?array $vars = []): array
    {
        return [
            'DATETIME'   => date('Y-m-d H:i:s (T)'),
            'TYPE'       => $errorType,
            'MSG'        => $errMsg,
            'SCRIPT'     => $fileName,
            'LINE'       => $lineNum,
            'SERVER'     => $_SERVER,
            'GET'        => $_GET,
            'POST'       => $_POST,
            'FILES'      => $_FILES,
            'STACKTRACE' => $this->getStackTrace(),
        ];
    }

    /**
     * @return string
     */
    private function getStackTrace(): string
    {
        return (new \Exception())->getTraceAsString();
    }
}
