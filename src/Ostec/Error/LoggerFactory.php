<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Class LoggerFactory
 *
 * @package Ostec\Error
 */
class LoggerFactory
{
    /**
     * @param string $logPath
     *
     * @return Logger
     */
    public static function getDefault(string $logPath = '/var/log/php/'): Logger
    {
        return new \Ostec\Error\Logger\Standard(
            new \Ostec\Error\Client\File($logPath),
            new \Ostec\Error\Aggregator\Standard(),
            new \Ostec\Error\Formatter\Text(),
            new \Ostec\Error\Filter\Standard()
        );
    }

    /**
     * @param string $host
     * @param int    $port
     *
     * @return Logger
     */
    public static function getUdpLogStash(string $host, int $port): Logger
    {
        return new Logger\Standard(
            new Client\Udp($host, $port),
            new Aggregator\Standard(),
            new Formatter\LogStash(),
            new Filter\Standard()
        );
    }
}
