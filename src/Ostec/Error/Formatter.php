<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Interface Formatter
 *
 * @package Ostec\Error
 */
interface Formatter
{
    /**
     * @param array|null $data
     *
     * @return string
     */
    public function format(?array $data = []): string;
}
