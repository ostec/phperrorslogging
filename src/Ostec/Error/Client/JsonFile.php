<?php declare(strict_types=1);

namespace Ostec\Error\Client;

use Ostec\Error\Client;

/**
 * Class JsonFile
 *
 * @package Ostec\Error\Client
 */
class JsonFile implements Client
{
    /**
     * @var string
     */
    private $logPath;

    /**
     * JsonFile constructor.
     *
     * @param string $logPath
     */
    public function __construct(string $logPath = '/var/log/php/')
    {
        if ($logPath{strlen($logPath) - 1} !== '/') {
            $logPath .= '/';
        }
        $this->logPath = $logPath;
    }

    /**
     * @param string $head
     * @param string $body
     *
     * @throws \Exception
     */
    public function send(string $head, string $body): void
    {
        $filename = $this->logPath.md5(microtime().random_int(0, 1000));
        file_put_contents($filename.'.json', $body);
    }
}

