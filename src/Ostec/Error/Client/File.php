<?php declare(strict_types=1);

namespace Ostec\Error\Client;

use Ostec\Error\Client;

/**
 * Class File
 *
 * @package Ostec\Error\Client
 */
class File implements Client
{
    /**
     * @var string
     */
    private $logPath;

    /**
     * File constructor.
     *
     * @param string $logPath
     */
    public function __construct(string $logPath = '/var/log/php/')
    {
        if ($logPath{strlen($logPath) - 1} !== '/') {
            $logPath .= '/';
        }
        $this->logPath = $logPath;
    }

    /**
     * @param string $head
     * @param string $body
     *
     * @throws \Exception
     */
    public function send(string $head, string $body): void
    {
        $filename = $this->logPath.md5(microtime().random_int(0, 1000));
        file_put_contents($filename.'.head.err', $head);
        file_put_contents($filename.'.body.err', $body);
    }
}

