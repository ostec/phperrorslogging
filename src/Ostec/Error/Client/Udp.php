<?php declare(strict_types=1);

namespace Ostec\Error\Client;

use Ostec\Error\Client;

/**
 * Class Udp
 *
 * @package Ostec\Error\Client
 */
class Udp implements Client
{
    /**
     * @var string
     */
    private $server;

    /**
     * @var int
     */
    private $port;

    /**
     * Udp constructor.
     *
     * @param string $server
     * @param int    $port
     */
    public function __construct(string $server, int $port)
    {
        $this->server = $server;
        $this->port   = $port;
    }

    /**
     * @param string $head
     * @param string $body
     */
    public function send(string $head, string $body): void
    {
        $fp = fsockopen(sprintf('udp://%s', $this->server), $this->port);

        if ($fp) {
            fwrite($fp, $body);
            fclose($fp);
        }
    }
}

