<?php declare(strict_types=1);

namespace Ostec\Error;

use Throwable;

/**
 * Class Handler
 *
 * @package Ostec\Error
 */
class Handler
{
    /**
     * @var Logger[]
     */
    private $logger;

    /**
     * @var Handler
     */
    private static $_instance;

    /**
     * Handler constructor.
     *
     * @param Logger[] $logger
     */
    private function __construct(array $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param int    $errno
     * @param string $errMsg
     * @param string $fileName
     * @param int    $lineNum
     * @param array  $vars
     */
    public function userErrorHandler(int $errno, string $errMsg, string $fileName, int $lineNum, array $vars = []): void
    {
        if (error_reporting() > 0) {
            foreach ($this->logger as $logger) {
                $logger->userErrorHandler($errno, $errMsg, $fileName, $lineNum, $vars);
            }
        }
    }

    /**
     * @param Throwable $exception
     */
    public function userExceptionHandler(Throwable $exception): void
    {
        foreach ($this->logger as $logger) {
            $logger->userExceptionHandler($exception);
        }
    }

    public function errorShutDownFunction(): void
    {
        $error = error_get_last();

        if (is_array($error) && $error['type'] === E_ERROR) {
            $this->userErrorHandler(
                $error['type'],
                $error['message'],
                $error['file'],
                $error['line']
            );
        }
    }

    /**
     * @return Handler
     */
    public static function getInstance(): Handler
    {
        return self::$_instance;
    }

    /**
     * @param Logger ...$logger
     *
     * @return Handler
     */
    public static function register(Logger ...$logger): Handler
    {
        $loggers = [];

        foreach ($logger as $log) {
            if ($log instanceof Logger) {
                $loggers[] = $log;
            }
        }

        if (count($loggers) === 0) {
            $loggers = [LoggerFactory::getDefault()];
        }

        $errorHandler = new Handler($loggers);

        ini_set('log_errors', '1');
        ini_set('display_errors', '0');

        register_shutdown_function(array($errorHandler, 'errorShutDownFunction'));
        set_error_handler(array($errorHandler, 'userErrorHandler'));
        set_exception_handler(array($errorHandler, 'userExceptionHandler'));

        self::$_instance = $errorHandler;

        return self::$_instance;
    }
}
