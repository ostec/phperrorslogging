<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Class LoggerBuilder
 *
 * @package Ostec\Error
 */
class LoggerBuilder
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Aggregator
     */
    private $aggregator;

    /**
     * @var Formatter
     */
    private $formatter;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @param \Ostec\Error\Client $client
     *
     * @return LoggerBuilder
     */
    public function setClient(Client $client): LoggerBuilder
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @param \Ostec\Error\Aggregator $aggregator
     *
     * @return LoggerBuilder
     */
    public function setAggregator(Aggregator $aggregator): LoggerBuilder
    {
        $this->aggregator = $aggregator;

        return $this;
    }

    /**
     * @param \Ostec\Error\Formatter $formatter
     *
     * @return LoggerBuilder
     */
    public function setFormatter(Formatter $formatter): LoggerBuilder
    {
        $this->formatter = $formatter;

        return $this;
    }

    /**
     * @param \Ostec\Error\Filter $filter
     *
     * @return LoggerBuilder
     */
    public function setFilter(Filter $filter): LoggerBuilder
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return Logger
     */
    public function build(): Logger
    {
        foreach (
            [
                'Client'     => $this->client,
                'Aggregator' => $this->aggregator,
                'Formater'   => $this->formatter,
                'Filter'     => $this->filter,
            ]
            as $key => $value
        ) {
            if (null === $value) {
                throw new \RuntimeException(sprintf('%s not set', $key));
            }
        }

        return new Logger\Standard(
            $this->client, $this->aggregator, $this->formatter, $this->filter
        );
    }
}
