<?php declare(strict_types=1);

namespace Ostec\Error\Logger;

use Ostec\Error\Aggregator;
use Ostec\Error\Aggregator\Exception;
use Ostec\Error\Client;
use Ostec\Error\Filter;
use Ostec\Error\Formatter;
use Ostec\Error\Logger;
use Throwable;

/**
 * Class Standard
 *
 * @package Ostec\Error\Logger
 */
class Standard implements Logger
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Aggregator
     */
    private $aggregator;

    /**
     * @var Formatter
     */
    private $formatter;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @param Client     $client
     * @param Aggregator $aggregator
     * @param Formatter  $formatter
     * @param Filter     $filter
     */
    public function __construct(Client $client, Aggregator $aggregator, Formatter $formatter, Filter $filter)
    {
        $this->client     = $client;
        $this->aggregator = $aggregator;
        $this->formatter  = $formatter;
        $this->filter     = $filter;
    }

    /**
     * @param int        $errno
     * @param string     $errMsg
     * @param string     $fileName
     * @param int        $lineNum
     * @param array|null $vars
     */
    public function userErrorHandler(int $errno, string $errMsg, string $fileName, int $lineNum, ?array $vars = []): void
    {
        if ($this->filter->filter($errno)) {
            $errorType = $this->filter->getName($errno);
            $this->client->send(
                $errorType.' - '.$errMsg,
                $this->formatter->format(
                    $this->aggregator->getMessage(
                        $errorType, $errno, $errMsg, $fileName, $lineNum, is_array($vars) ? $vars : []
                    )
                )
            );
        }
    }

    /**
     * @param Throwable $exception
     */
    public function userExceptionHandler(Throwable $exception): void
    {
        $ag = new Exception($this->aggregator);
        $this->client->send(
            'Exception - '.$exception->getMessage(),
            $this->formatter->format(
                $ag->getMessage($exception)
            )
        );
    }
}
