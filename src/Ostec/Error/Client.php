<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Interface Client
 *
 * @package Ostec\Error
 */
interface Client
{
    /**
     * @param string $head
     * @param string $body
     */
    public function send(string $head, string $body): void;
}
