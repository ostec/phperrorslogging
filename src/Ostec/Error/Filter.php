<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Interface Filter
 *
 * @package Ostec\Error
 */
interface Filter
{
    /**
     * @param int $errorType
     *
     * @return bool
     */
    public function filter(int $errorType): bool;

    /**
     * @param int $errorType
     *
     * @return string
     */
    public function getName(int $errorType): string;
}

