<?php declare(strict_types=1);

namespace Ostec\Error;

/**
 * Interface Aggregator
 *
 * @package Ostec\Error
 */
interface Aggregator
{
    /**
     * @param string $errorType
     * @param int    $errno
     * @param string $errMsg
     * @param string $fileName
     * @param int    $lineNum
     * @param array  $vars
     *
     * @return array
     */
    public function getMessage(string $errorType, int $errno, string $errMsg, string $fileName, int $lineNum, ?array $vars = []): array;
}
