<?php declare(strict_types=1);

namespace Ostec\Error\Formatter;

use Ostec\Error\Formatter;

/**
 * Class Text
 *
 * @package Ostec\Error\Formatter
 */
class Text implements Formatter
{
    /**
     * @param array|null $data
     *
     * @return string
     */
    public function format(?array $data = []): string
    {
        $err = '';

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $err .= sprintf('%s => %s', $key, print_r($value, true));
            } else {
                $err .= sprintf('%s:%s', $key, $value);
            }
            $err .= chr(10);
        }

        return $err;
    }
}
