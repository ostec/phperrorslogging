<?php declare(strict_types=1);

namespace Ostec\Error\Formatter;

/**
 * Class Logstash
 *
 * @package Ostec\Error\Formater
 */
class LogStash extends Json
{
    /**
     * @var array
     */
    private $additionalData;

    /**
     * Logstash constructor.
     *
     * @param array $additionalData
     */
    public function __construct(array $additionalData = [])
    {
        $this->additionalData = $additionalData;
    }

    /**
     * @param array|null $data
     *
     * @return string
     */
    public function format(?array $data = []): string
    {
        $data               = array_merge($this->additionalData, $data);
        $data['@timestamp'] = date('c');
        $data['level']      = $data['TYPE'];

        return parent::format($data);
    }
}
