<?php declare(strict_types=1);

namespace Ostec\Error\Formatter;

use Ostec\Error\Formatter;

/**
 * Class Json
 *
 * @package Ostec\Error\Formatter
 */
class Json implements Formatter
{
    /**
     * @param array|null $data
     *
     * @return string
     */
    public function format(?array $data = []): string
    {
        if (false !== ($return = json_encode($data, 20))) {
            return $return;
        }

        return serialize($data);
    }
}
