<?php
ini_set('date.timezone', 'europe/berlin');
include __DIR__.'/../vendor/autoload.php';

use Ostec\Error\Aggregator\Standard;
use Ostec\Error\Client\File;
use Ostec\Error\Formatter\LogStash;
use Ostec\Error\Handler;
use Ostec\Error\LoggerBuilder;

$builder = new LoggerBuilder();

Handler::register(
    $builder
        ->setClient(new File('/tmp/errors'))
        ->setAggregator(new Standard())
        ->setFormatter(new LogStash(['project' => 'frontend']))
        ->setFilter(new \Ostec\Error\Filter\Standard())
        ->build()
);

@file('notexists') || print PHP_EOL.'not exists'.PHP_EOL;

/*

try {
    $string = 'Garmin';

    $date = new DateTime($string, new DateTimeZone('Europe/Berlin'));

} catch (Exception $exception) {
    print $exception->getMessage();
}
*/
print PHP_EOL;
